Must Have

Request car share

VARIABLES
client
destination
pickup time
number of passengers
number of available cars
pickup location
car reservation
list of cars
availability status

-----------
Should Have

Share your car

VARIABLES
list your car
car make
car model
car value
number of seats
request approval


-----------
Could Have

Login system
Auto calculate car destination distance/price
-----------
Won't Have (this time)
locations globally (more location specific)
Interactive map
Premium service (super cars)
Self driving cars (Hands free)
-----------
